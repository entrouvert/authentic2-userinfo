from django.conf.urls import patterns, url

from authentic2.decorators import setting_enabled, required

from . import app_settings
from .views import UserFullData

urlpatterns = required(
        setting_enabled('ENABLED', settings=app_settings),
        patterns('',
                 url('^userinfo/(?P<nameid>_\w+)$', UserFullData.as_view(),
                name='authentic2-userinfo'),
        )
)
