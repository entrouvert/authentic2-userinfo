import datetime
import base64
import hmac
import hashlib
import urllib
import random
import urlparse

def sign_string(s, key, algo='sha256', timedelta=30):
    digestmod = getattr(hashlib, algo)
    hash = hmac.HMAC(key, digestmod=digestmod, msg=s)
    return hash.digest()

def check_query(query, key, known_nonce=None, timedelta=30):
    parsed = urlparse.parse_qs(query)
    signature = base64.b64decode(parsed['signature'][0])
    algo = parsed['algo'][0]
    timestamp = parsed['timestamp'][0]
    timestamp = datetime.datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%SZ')
    nonce = parsed['nonce']
    unsigned_query = query.split('&signature=')[0]
    if known_nonce is not None and known_nonce(nonce):
        return False
    if abs(datetime.datetime.utcnow() - timestamp) > datetime.timedelta(seconds=timedelta):
        return False
    return check_string(unsigned_query, signature, key, algo=algo)

def check_string(s, signature, key, algo='sha256'):
    # constant time compare
    signature2 = sign_string(s, key, algo=algo)
    if len(signature2) != len(signature):
        return False
    res = 0
    for a, b in zip(signature, signature2):
        res |= ord(a) ^ ord(b)
    return res == 0

if __name__ == '__main__':
    test_key = '12345'
    signed_query = sign_query('NameId=_12345&orig=montpellier', test_key)
    assert check_query(signed_query, test_key, timedelta=0) is False
    assert check_query(signed_query, test_key) is True
