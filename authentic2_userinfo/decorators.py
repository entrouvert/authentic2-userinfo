from urlparse import urlparse
from functools import wraps

from django.http import HttpResponseForbidden

from . import app_settings, signature

def valid_signature(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        api_key = app_settings.API_KEY
        request =  args[1]
        signed_query = urlparse(request.get_full_path()).query
        try:
            assert signature.check_query(signed_query, api_key) is True
            return func(*args, **kwargs)
        except (KeyError, AssertionError):
            return HttpResponseForbidden()
    return wrapper
