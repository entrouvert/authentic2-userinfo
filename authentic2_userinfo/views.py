from django.views.generic import View

from jsonresponse import to_json

from authentic2 import compat

from .decorators import valid_signature

user_model = compat.get_user_model()

class UserFullData(View):

    @valid_signature
    @to_json('api')
    def get(self, request, nameid):
        user = user_model.objects.get(libertyfederation__name_id_content=nameid, deleteduser__isnull=True)
        return {'first_name': user.first_name, 'last_name': user.last_name,
                'email': user.email}
