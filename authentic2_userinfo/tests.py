import json

from django.test import TestCase, Client
from django.core.urlresolvers import reverse

class UserInfoTest(TestCase):

    fixtures = ('users.json', 'federations.json', 'deleted_users.json')

    def setUp(self):
        self.client = Client()

    def test_existing_user(self):
        response = self.client.get(reverse('authentic2-userinfo',
                                           kwargs={'nameid':'_E191B750CC87587E6E2ED2130262C4F2'}))
        self.assertEqual(response.status_code, 200)
        try:
            data = json.loads(response.json)
        except:
            self.fail('response not a json')

        self.assertEqual(data['email'], 'admin@entrouvert.com')

    def test_deleted_user(self):
        response = self.client.get(reverse('authentic2-userinfo',
                                           kwargs={'nameid':'_05D1286DA257FB1326C12107576D628B'}))
        self.assertEqual(response.status_code, 404)

    def test_nonexisting_user(self):
        response = self.client.get(reverse('authentic2-userinfo',
                                           kwargs={'nameid':'_05D12DFGZVA257FB1326C12107576D628B'}))
        self.assertEqual(response.status_code, 404)
